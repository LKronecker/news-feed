//
//  CBCNewsCollectionVIewCell.swift
//  CBC_News
//
//  Created by Leopoldo Garcia Vargas on 2016-09-24.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import UIKit
import Foundation
import AlamofireImage

class NFNewsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var author: UILabel!
    @IBOutlet var title: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var image: UIImageView!
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.masksToBounds = true;
        
        self.image.layer.borderWidth = 3
        self.image.layer.cornerRadius = 45
        self.image.layer.masksToBounds = true
        self.image.layer.borderColor = UIColor.darkGray.cgColor;
        
    }
    
    override func prepareForReuse() {
        self.image.image = UIImage(named:"placeholder")
    }
    
    public func update(story:NFStoryModel){
        print("update")
        self.author.text! = story.author!
        self.title.text! = story.title!
        self.date.text = story.publicationDate!
        self.image.af_setImage(withURL: NSURL(string:story.image!) as! URL)
    }
}

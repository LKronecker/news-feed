//
//  NFDetailViewController.swift
//  CBC_News
//
//  Created by Leopoldo Garcia Vargas on 2016-09-24.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import Foundation
import UIKit

class NFDetailViewController:  UIViewController, UIWebViewDelegate{
    
    var story: NFStoryModel;
    @IBOutlet var webView: UIWebView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    // MARK.- Initializers
    
    required init(coder aDecoder: (NSCoder!)) {
        self.story = NFStoryModel()
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        self.story = NFStoryModel()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience init(story:NFStoryModel) {
        self.init(nibName: nil, bundle: nil)
        self.story = story
        
    }
    
    // Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.startAnimating()
        
        self.webView.delegate = self
        let url = URL (string: self.story.link!)
        let requestObj = URLRequest(url: url!)
        self.webView.loadRequest(requestObj)
    }
    
    func webViewDidFinishLoad(_ webView : UIWebView) {
        self.activityIndicator.stopAnimating()
    }
}

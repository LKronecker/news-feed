//
//  ViewController.swift
//  CBC_News
//
//  Created by Leopoldo Garcia Vargas on 2016-09-24.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import UIKit

class NFCollectionViewController: UICollectionViewController {

    var storyList = [NFStoryModel]()
    let newsCellIdentifier = "newsCellIdentifier"
    
    override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
        self.collectionView?.register(UINib.init(nibName: "NFNewsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: self.newsCellIdentifier)
    }
    
    convenience init(storyList:[NFStoryModel]) {
        let layout = UICollectionViewFlowLayout()
        self.init(collectionViewLayout: layout)
        self.storyList = storyList
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.collectionView?.showsVerticalScrollIndicator = false
        
        self.collectionView?.backgroundColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.storyList.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.newsCellIdentifier, for: indexPath) as! NFNewsCollectionViewCell
        cell.update(story: self.storyList[indexPath.row])
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let story = self.storyList[(indexPath as NSIndexPath).row]
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let storyDetailsVC = storyboard.instantiateViewController(withIdentifier: "detailview") as! NFDetailViewController
        storyDetailsVC.story = story;
        self.navigationController?.pushViewController(storyDetailsVC, animated: true)
    }
    
    //Mark: Flow Layout
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let collectionSize: CGRect = (self.view?.frame)!
        return CGSize(width: collectionSize.width, height: 150);
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }
    
    // Mark - Alert    
    fileprivate func presentLoginAlert(title:String, body:String) -> Void {
        
        let alertController = UIAlertController(title: title, message: body, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.collectionView?.reloadData()
    }

}


//
//  CBCNewsModel.swift
//  CBC_News
//
//  Created by Leopoldo Garcia Vargas on 2016-09-24.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import Foundation

class NFStoryModel {
    var title: String?
    var author: String?
    var publicationDate: String??
    var image: String?
    var link: String?
}

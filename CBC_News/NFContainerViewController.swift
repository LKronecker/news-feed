//
//  NFContainerViewController.swift
//  CBC_News
//
//  Created by Leopoldo Garcia Vargas on 2016-09-24.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import SnapKit
import UIKit

class NFContainerViewController : UIViewController {
    
    var newsFeedViewController = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.title = "News Feed"
        
        NFDataBroker.sharedInstance.getTopStories {(NSDictionary, NSError) in
            if ((NSError) != nil) {
                self.presentLoginAlert(title: "Failure", body: "There's a problem with the internet, please verify your connection.")
            } else {
                //Present Feed Colletion View
                self.newsFeedViewController = NFCollectionViewController(storyList:NSDictionary?["topstories"] as! [NFStoryModel])
                self.addViewControllerAsChildViewController(viewController: self.newsFeedViewController)
            }
        }
    }
    
    // Add Child View Controller
    private func addViewControllerAsChildViewController(viewController: UIViewController) {
        addChildViewController(viewController)
        view.addSubview(viewController.view)

        viewController.didMove(toParentViewController: self)
        // Constraints
        viewController.view.snp.makeConstraints { (make) -> Void in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top:(self.navigationController?.navigationBar.frame.height)! + 30, left:10, bottom:10, right:10))
        }
    }
    
    // Mark - Alert
    
    fileprivate func presentLoginAlert(title:String, body:String) -> Void {
        
        let alertController = UIAlertController(title: title, message: body, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

//
//  CBCDataBroker.swift
//  CBC_News
//
//  Created by Leopoldo Garcia Vargas on 2016-09-24.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyXMLParser

typealias ServiceResponse = (NSDictionary?, NSError?) -> Void

class NFDataBroker {
    
    let BASE_URL = "http://www.cbc.ca/cmlink/rss-topstories"
    
    class var sharedInstance:NFDataBroker {
        struct Singleton {
            static let instance = NFDataBroker()
        }
        return Singleton.instance
    }
    
    func getTopStories(onCompletion: @escaping ServiceResponse) {
        let URL = BASE_URL
        Alamofire.request(URL)
            .responseData { response in
                if let data = response.data {
                    let xml = XML.parse(data)
                    var storyList = [NFStoryModel]()
                    
                    for storyXML in xml["rss", "channel", "item"] {
                        let model = NFStoryModel()
                        model.title = storyXML["title"].text
                        model.author = storyXML["author"].text
                        model.publicationDate = storyXML["pubDate"].text
                        model.link = storyXML["link"].text
                        //Image
                        let imageData = storyXML["description"].text?.data(using: String.Encoding.utf8)
                        let imageXml = XML.parse(imageData!)["img"]
                        model.image = imageXml.attributes["src"]

                        storyList.append(model)
                    }
                    let responseDict = ["topstories":storyList] as NSDictionary
                    onCompletion(responseDict, nil)
                }
                else {
                    onCompletion(nil, NSError())
                }
        }
    }
}

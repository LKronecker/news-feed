This project is a News Feed iOS App written in Swift 3.0 on Xcode 8.0.

This demo app is a small project that fetches a list of news stories and displays them into a collection view. It also allows the user to select one of the items in the list to be able to read the full story in a detail view page. The detail page is loaded on a UIWebView.

The App fetches its data from : http://www.cbc.ca/cmlink/rss-topstories in XML format.

Architecture.

The application makes use of different patterns to achieve it’s goals. 
MVC: Used to distribute work in the news feed collection view controller. The cell is the view, the story model the model and the collection view controller would be the controller.
Delegate/Protocol: To establish communication between several VCs without creating direct dependencies.
Singleton service providers: the datastore and data broker are Singleton objects that are accessible through all the app.
View controller containment: The news feed view contains the news feed collection view controller as a child to delegate responsibilities properly and to distribute code more clearly.